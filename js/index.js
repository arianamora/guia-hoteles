 //activando tooltip, popover y carousel
 $(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        iterval:2000
        });

    //eventos
    
    $('#contacto').on('show.bs.modal',function(e){
        console.log('el modal se esta mostrando');
        //quitar la clase
        $('#contactoBtn').removeClass('btn-outline-success');
        //agregar clase
        $('#contactoBtn').addClass('btn-primary');
        //deshabilitar
        $('#contactoBtn').prop('disabled,true');
    });
    $('#contacto').on('shown.bs.modal',function(e){
        console.log('el modal se mostro');
    });
    $('#contacto').on('hide.bs.modal',function(e){
        console.log('el modal se oculta');
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log('el modal se oculto');
        //habilitar
        $('#contactoBtn').prop('disabled,false');
    });
});